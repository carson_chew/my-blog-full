<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thread_model extends CI_Model {

	// New thread profile
	public function new_thread_profile($foo_array){

		$sql = $this->db->insert('topics', $foo_array);

		if ($sql == true) {
			return true;
		} else {
			return false;
		}
	}


	// Update thread profile
	public function update_thread_profile($tp_id, $foo_array){

		$this->db->where('tp_id', $tp_id);
		$sql = $this->db->update('topics', $foo_array);

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Delete thread profile
	public function delete_thread_profile($tp_id){

		$this->db->where('tp_id', $tp_id);
		$sql = $this->db->delete('topics');

		if ($this->db->affected_rows() > 0) {

			// Delete reply
			$this->db->where('tc_tp_id', $tp_id);
			$delete_reply = $this->db->delete('topic_comment');

			return true;
		}else{
			return false;
		}
	}


	// Single thread profile
	public function single_thread_profile($tp_id){

		$this->db->where('tp_id', $tp_id);
		$this->db->where('up_id = tp_up_id');
		$sql = $this->db->get('topics, user_profile', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->row();
		} else {
			return false;
		}
	}


	// show all the thread
	public function all_thread($keyword = null){

		if ($keyword != null) {
			$this->db->like('tp_title', $keyword);
		}

		$this->db->order_by('tp_id', 'desc');
		$this->db->where('up_id = tp_up_id');
		$sql = $this->db->get('topics, user_profile', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->result();
		} else {
			return false;
		}
	}


	// Show the max reply topic
	public function all_max_reply_topic(){

		$this->db->limit(3);
		$this->db->order_by('ranking', 'desc');
		$this->db->group_by('tc_tp_id');
		$this->db->where('tp_id = tc_tp_id');
		$this->db->select('COUNT(tc_tp_id) as ranking, tp_id, tp_title');
		$sql = $this->db->get('topics, topic_comment', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->result();
		} else {
			return false;
		}
	}




	// ==================================================================
	//
	// Reply Comment
	//
	// ------------------------------------------------------------------



	// show all reply of thread
	public function all_reply($tp_id){

		$this->db->order_by('tc_id', 'desc');
		$this->db->where('tc_tp_id', $tp_id);
		$this->db->where('up_id = tc_up_id');
		$this->db->where('tp_id = tc_tp_id');
		$sql = $this->db->get('topics, user_profile, topic_comment', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->result();
		} else {
			return false;
		}
	}


	// New reply profile
	public function new_reply_profile($foo_array){

		$sql = $this->db->insert('topic_comment', $foo_array);

		if ($sql == true) {
			return true;
		} else {
			return false;
		}
	}


	// Update reply profile
	public function update_reply_profile($rp_id, $foo_array){

		$this->db->where('tc_id', $rp_id);
		$sql = $this->db->update('topic_comment', $foo_array);

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Delete reply profile
	public function delete_reply_profile($rp_id){

		$this->db->where('tc_id', $rp_id);
		$sql = $this->db->delete('topic_comment');

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}




}

/* End of file Thread_model.php */
/* Location: ./application/models/Thread_model.php */
