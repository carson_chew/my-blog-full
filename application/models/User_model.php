<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	// New user profile
	public function new_user_profile($foo_array){

		$sql = $this->db->insert('user_profile', $foo_array);

		if ($sql == true) {
			return true;
		} else {
			return false;
		}
	}


	// Update user profile
	public function update_user_profile($up_id, $foo_array){

		$this->db->where('up_id', $up_id);
		$sql = $this->db->update('user_profile', $foo_array);

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}


	// Single user profile
	public function single_user_profile($up_id){

		$this->db->where('up_id', $up_id);
		$sql = $this->db->get('user_profile', null, null);

		if ($sql->num_rows() != 0) {
			return $sql->row();
		} else {
			return false;
		}
	}


	// User profile with username
	public function user_profile_by_usrname($username){

		$this->db->where('up_name', $username);
		$sql = $this->db->get('user_profile', null, null);

		if ($sql->num_rows() > 0) {
			return $sql->row();
		} else {
			return false;
		}
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
