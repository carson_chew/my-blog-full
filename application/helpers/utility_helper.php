<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Display message
function error_msg(){

	$ci =&get_instance();

	$custom_message_notif = $ci->session->flashdata('msg_notification');

	//For msg that not inside the db
	if (isset($custom_message_notif) && !empty($custom_message_notif)) {
		customize_message_handler($custom_message_notif);
	}
}



//Get message UI - Customize
function customize_message_handler($custom_message_notif){

	if ($custom_message_notif['msg_type'] == 'pass') {
		echo '<div class="alert alert-success alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <i class="fas fa-check"></i> '.$custom_message_notif['msg_desc'].'
	          </div>';
	} else {
		echo '<div class="alert alert-danger alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <i class="far fa-times-circle"></i> '.$custom_message_notif['msg_desc'].'
	          </div>';
	}
}


// Get the user id
function user_id(){

	$ci =&get_instance();

	$user_id = $ci->session->userdata("user_id");

	return $user_id;
}



/**
 * Check user session
 */
function user_is_logged(){

	$ci =&get_instance();

	$log_status = $ci->session->userdata("user_logged_in");
	$user_id    = user_id();

	if ($log_status != true) {
		$ci->session->set_flashdata('msg_notification', array("msg_desc" => "Please login to continue.", "msg_type" => "failed"));
		redirect('home', 'location', null);
		exit();
	}
}
