<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thread_process extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('thread_model');
	}


	// New topic
	public function post_new_thread(){

		$post_topic   = $this->input->post('post_topic');
		$post_content = $this->input->post('post_content');

		$date = date('Y-m-d H:i:s');

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_new_post'])){

			$topic_data = array(
				"tp_up_id"        => user_id(),
				"tp_title"        => $post_topic,
				"tp_body"         => $post_content,
				"tp_created_date" => $date
			);

			$sql = $this->thread_model->new_thread_profile($topic_data);

			if ($sql == true) {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "New thread successfully created.", "msg_type" => "pass"));
				redirect('home', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Thread failed to create. Please try again!", "msg_type" => "failed"));
				redirect('home', 'location', null);
				exit();
			}
		}
	}



	// Edit topic
	public function edit_thread($tp_id){

		$post_topic   = $this->input->post('post_topic');
		$post_content = $this->input->post('post_content');

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_edit_post'])){

			$topic_data = array(
				"tp_title" => $post_topic,
				"tp_body"  => $post_content
			);

			$sql = $this->thread_model->update_thread_profile($tp_id, $topic_data);

			if ($sql == true) {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data has been save.", "msg_type" => "pass"));
				redirect('home', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data failed to save. Please try again!", "msg_type" => "failed"));
				redirect('home', 'location', null);
				exit();
			}
		}
	}


	// Delete topic
	public function delete_thread($tp_id){

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_delete_post'])){

			$sql = $this->thread_model->delete_thread_profile($tp_id);

			if ($sql == true) {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data has been deleted.", "msg_type" => "pass"));
				redirect('home', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data failed to delete. Please try again!", "msg_type" => "failed"));
				redirect('home', 'location', null);
				exit();
			}
		}
	}


	// ==================================================================
	//
	// Reply
	//
	// ------------------------------------------------------------------



	// New Reply
	public function new_reply($tp_id){

		$post_content = $this->input->post('post_content');

		$date = date('Y-m-d H:i:s');

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_new_reply'])){

			$topic_data = array(
				"tc_tp_id"        => $tp_id,
				"tc_up_id"        => user_id(),
				"tc_content"      => $post_content,
				"tc_created_date" => $date
			);

			$sql   = $this->thread_model->new_reply_profile($topic_data);
			$rp_id = $this->db->insert_id();

			if ($sql == true) {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data has been saved.", "msg_type" => "pass"));
				redirect('topic_profile/'.$tp_id, 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data failed to save. Please try again!", "msg_type" => "failed"));
				redirect('topic_profile/'.$tp_id, 'location', null);
				exit();
			}
		}
	}


	// Edit Reply
	public function edit_reply($tp_id, $rp_id){

		$post_content = $this->input->post('post_content');

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_edit_reply'])){

			$topic_data = array(
				"tc_content" => $post_content
			);

			$sql = $this->thread_model->update_reply_profile($rp_id, $topic_data);

			if ($sql == true) {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data has been saved.", "msg_type" => "pass"));
				redirect('topic_profile/'.$tp_id, 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data failed to save. Please try again!", "msg_type" => "failed"));
				redirect('topic_profile/'.$tp_id, 'location', null);
				exit();
			}
		}
	}


	// Delete Reply
	public function delete_reply($tp_id, $rp_id){

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_delete_reply'])){

			$sql = $this->thread_model->delete_reply_profile($rp_id);

			if ($sql == true) {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data has been deleted.", "msg_type" => "pass"));
				redirect('topic_profile/'.$tp_id, 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Data failed to delete. Please try again!", "msg_type" => "failed"));
				redirect('topic_profile/'.$tp_id, 'location', null);
				exit();
			}
		}
	}
}

/* End of file Thread_process.php */
/* Location: ./application/controllers/Thread_process.php */
