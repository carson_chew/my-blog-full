<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic_profile extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('thread_model');
	}

	public function index(){

		// Thread ID
		$topic_id             = $this->uri->segment(2);
		$data['topic_id']     = $topic_id;


		// Topic profile
		$data['topic_profile'] = $this->thread_model->single_thread_profile($topic_id);

		// Show all reply
		$data['all_reply'] = $this->thread_model->all_reply($topic_id);

		$this->load->view('topic_profile_view', $data);
	}

}

/* End of file Topic_profile.php */
/* Location: ./application/controllers/Topic_profile.php */
