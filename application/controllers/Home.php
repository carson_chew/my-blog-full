<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('thread_model');
	}

	public function index(){

		// Get the search keyword
		$keyword         = $this->input->get('keyword');
		$data['keyword'] = $keyword;

		// Show all post
		$data['all_post'] = $this->thread_model->all_thread($keyword);

		// Show top 3 comment
		$data['top_reply'] = $this->thread_model->all_max_reply_topic();

		$this->load->view('home_view', $data);
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */
