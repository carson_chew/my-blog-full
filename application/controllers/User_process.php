<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_process extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('user_model');
	}


	// New Account
	public function create_user_account(){

		$usr_name         = $this->input->post('usr_name');
		$usr_pass         = $this->input->post('usr_pass');
		$usr_pass_confirm = $this->input->post('usr_pass_confirm');

		// Hashed password
		$user_hashed = password_hash($usr_pass, PASSWORD_DEFAULT);

		// Force user key in password again
		if ($usr_pass != $usr_pass_confirm) {
			$this->session->set_flashdata('msg_notification', array("msg_desc" => "Password Not Match. Please Try Again.", "msg_type" => "failed"));
			redirect('home', 'location', null);
			exit();
		}

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_new_account'])){

			$usr_data = array(
				"up_name"     => $usr_name,
				"up_password" => $user_hashed
			);

			$sql     = $this->user_model->new_user_profile($usr_data);
			$user_id = $this->db->insert_id();

			if ($sql == true) {

				// Create user session
				$user_data = array(
					'user_name'             => $usr_name,
					'user_id'               => $user_id,
					'user_logged_in'        => true
				);

				$this->session->set_userdata($user_data);

				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Account successfully created. Thank you for your registration!", "msg_type" => "pass"));
				redirect('home', 'location', null);
				exit();
			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "Account failed to create. Please try again!", "msg_type" => "pass"));
				redirect('home', 'location', null);
				exit();
			}
		}
	}


	// Login
	public function user_login(){

		$usr_name = $this->input->post('usr_name');
		$usr_pass = $this->input->post('usr_pass');

		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit_login'])){

			$user_info = $this->user_model->user_profile_by_usrname($usr_name);

			if ($user_info != false) {

				$user_password = $user_info->up_password;
				$user_id       = $user_info->up_id;
				$user_name     = $user_info->up_name;

				if (password_verify($usr_pass, $user_password)) {

					// Simple user profile session
					$user_data = array(
						'user_name'             => $user_name,
						'user_id'               => $user_id,
						'user_logged_in'        => true
					);

					$this->session->set_userdata($user_data);

					redirect('home', 'location', null);
					exit();
				} else {
					$this->session->set_flashdata('msg_notification', array("msg_desc" => "	Couldn't find the username or wrong password.", "msg_type" => "failed"));
					redirect('home', 'location', null);
					exit();
				}

			} else {
				$this->session->set_flashdata('msg_notification', array("msg_desc" => "	Couldn't find the username.", "msg_type" => "failed"));
				redirect('home', 'location', null);
				exit();
			}
		}
	}


	// Logout
	public function logout(){
		$this->session->sess_destroy();
		redirect('home', 'location', null);
		exit();
	}
}

/* End of file User_process.php */
/* Location: ./application/controllers/User_process.php */
