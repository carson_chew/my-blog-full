<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>My First Blog | Home</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url("bootstrap/plugins/fontawesome-free/css/all.min.css"); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("bootstrap/dist/css/adminlte.min.css"); ?>">

  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url("bootstrap/plugins/summernote/summernote-bs4.css"); ?>">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="<?= base_url("home"); ?>" class="navbar-brand">
        <img src="<?= base_url("bootstrap/dist/img/AdminLTELogo.png"); ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">My Blog</span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">

          <li class="nav-item">
            <a href="<?= base_url("home"); ?>" class="nav-link">Home</a>
          </li>

          <?php if (user_id() > 0): ?>
            <li class="nav-item">
              <a href="#" class="nav-link" data-toggle="modal" data-target="#newPost">New Post</a>
            </li>
          <?php else: ?>
            <li class="nav-item">
              <a href="#" class="nav-link" data-toggle="modal" data-target="#login" title="Please login before post">New Post</a>
            </li>
          <?php endif ?>
        </ul>


        <!-- SEARCH FORM -->
        <form class="form-inline ml-0 ml-md-3" method="GET" action="">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" name="keyword">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit" name="submit_search_keyword">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <?php if (user_id() > 0): ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("user_process/logout"); ?>">
              <i class="fas fa-sign-out-alt"></i>
              Logout
            </a>
          </li>
        <?php else: ?>
          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#signUp">
              <i class="fas fa-user"></i>
              Create Account
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#login">
              <i class="fas fa-sign-in-alt"></i>
              Login
            </a>
          </li>
        <?php endif ?>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">

        <?= error_msg(); ?>

        <?php if (!empty($keyword)): ?>
          <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Keyword: </strong> <?= $keyword; ?>
            <br><a href="<?= base_url("home"); ?>">Reset</a>
          </div>
        <?php endif ?>

        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Latest Threads</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">

          <div class="col-lg-8">

            <?php if ($all_post == false): ?>
              <p>No Threads Found.</p>
            <?php else: ?>
              <?php foreach ($all_post as $key => $post): ?>

                <div class="card card-primary card-outline">
                  <div class="card-header">
                    <h5 class="card-title">#<?= $key+1; ?></h5>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title"><?= $post->tp_title; ?></h6>

                    <p class="card-text">
                      <?= $post->tp_body; ?>
                      <br><small>Posted Date: <?= $post->tp_created_date; ?> [<?= $post->up_name; ?>]</small>
                    </p>

                    <?php if (user_id() == $post->tp_up_id): ?>
                      <p>
                        <a href="#" data-toggle="modal" data-target="#editPost<?= $key; ?>"><i class="far fa-edit"></i> Edit</a> &nbsp;
                        <a href="#" data-toggle="modal" data-target="#deletePost<?= $key; ?>"><i class="fas fa-trash-alt"></i> Delete</a>
                      </p>

                      <div class="modal fade" id="editPost<?= $key; ?>">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <form action="<?= base_url("thread_process/edit_thread/".$post->tp_id); ?>" method="POST">

                              <div class="modal-body">
                                <div class="form-group">
                                  <label for="post_topic">Topic</label>
                                  <input type="text" class="form-control" id="post_topic" name="post_topic" value="<?= $post->tp_title; ?>">
                                </div>

                                <textarea name="post_content" class="textarea" id="post_content" rows="10"><?= $post->tp_body; ?></textarea>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" name="submit_edit_post" class="btn btn-primary">Save Changes</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal fade" id="deletePost<?= $key; ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Edit</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <form action="<?= base_url("thread_process/delete_thread/".$post->tp_id); ?>" method="POST">
                              <div class="modal-body">
                                <p>Do you want to delete this record? Once deleted cannot undo it.</p>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" name="submit_delete_post" class="btn btn-danger">Delete</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                    <?php endif ?>

                    <a href="<?= base_url("topic_profile/".$post->tp_id); ?>" class="btn btn-primary">Read More</a>
                  </div>
                </div>

              <?php endforeach ?>
            <?php endif ?>

          </div>
          <!-- /.col-md-6 -->

          <div class="col-lg-4">

            <div class="card card-danger card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">Top 3 Comment Threads</h5>
              </div>
              <div class="card-body">
                <?php if ($top_reply == false): ?>
                  <p>No Ranking Found</p>
                <?php else: ?>
                  <ul>
                    <?php foreach ($top_reply as $trKey => $top): ?>
                      <li><a href="<?= base_url("topic_profile/".$top->tp_id); ?>"><?= $top->tp_title; ?> [<?= $top->ranking; ?>]</a></li>
                    <?php endforeach ?>
                  </ul>
                <?php endif ?>
              </div>
            </div>

          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline"></div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->




<!-- New Post Modal -->
<div class="modal fade" id="newPost">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">New Post</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url("thread_process/post_new_thread"); ?>" method="POST">

        <div class="modal-body">
          <div class="form-group">
            <label for="post_topic">Topic</label>
            <input type="text" class="form-control" id="post_topic" name="post_topic" placeholder="What is the topic?">
          </div>

          <textarea name="post_content" class="textarea" id="post_content" rows="10"></textarea>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="submit_new_post" class="btn btn-primary">Post</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- New User Modal -->
<div class="modal fade" id="signUp">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Join Us</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url("user_process/create_user_account"); ?>" method="POST">
        <div class="modal-body">

          <div id="msg_box" style="display: none;">
            <div class="alert alert-danger" id="error_msg_box"></div>
          </div>

          <div class="form-group">
            <label for="usr_name">Username</label>
            <input type="text" class="form-control" id="usr_name" name="usr_name" required>
          </div>

          <div class="form-group">
            <label for="usr_pass">Password</label>
            <input type="password" class="form-control" id="usr_pass" name="usr_pass" required>
          </div>

          <div class="form-group">
            <label for="usr_pass_confirm">Confirm Password</label>
            <input type="password" class="form-control" id="usr_pass_confirm" name="usr_pass_confirm" required>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="submit_new_account" class="btn btn-primary">Sign Up</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- Login Modal -->
<div class="modal fade" id="login">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Login</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url("user_process/user_login"); ?>" method="POST">
        <div class="modal-body">

          <div class="form-group">
            <label for="usr_name">Username</label>
            <input type="text" class="form-control" id="usr_name" name="usr_name" required>
          </div>

          <div class="form-group">
            <label for="usr_pass">Password</label>
            <input type="password" class="form-control" id="usr_pass" name="usr_pass" required>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="submit_login" class="btn btn-primary">Login</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- jQuery -->
<script src="<?= base_url("bootstrap/plugins/jquery/jquery.min.js"); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url("bootstrap/plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url("bootstrap/dist/js/adminlte.min.js"); ?>"></script>

<!-- Summernote -->
<script src="<?= base_url("bootstrap/plugins/summernote/summernote-bs4.min.js"); ?>"></script>
<script src="<?= base_url("bootstrap/custom/home.js"); ?>"></script>

</body>
</html>
