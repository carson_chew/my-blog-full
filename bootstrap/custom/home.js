$(function() {

	// Do password checking
	$("#usr_pass_confirm").on('keyup', function(event) {
		event.preventDefault();

		var first_pass  = $("#usr_pass").val();
		var second_pass = $("#usr_pass_confirm").val();

		if(first_pass != second_pass){
			$("#msg_box").css('display', 'block');
			$("#error_msg_box").html("Password not match.");
		}else{
			$("#msg_box").css('display', 'none');
		}
	});

	// Summernote
	$('.textarea').summernote({
		height: 280,
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		]
	});
});


$('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});
